#!/bin/bash
user=root

date
pids=""

(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=1 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-1) &
pids+=" $!"
(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=2 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-2) &
pids+=" $!"
(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=3 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-3) &
pids+=" $!"
(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=4 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-4) &
pids+=" $!"
(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=5 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-5) &
pids+=" $!"
(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=6 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-6) &
pids+=" $!"
(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=7 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-7) &
pids+=" $!"
(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=8 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-8) &
pids+=" $!"
(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=11 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-9) &
pids+=" $!"
(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=12 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-10) &
pids+=" $!"
(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=13 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-11) &
pids+=" $!"
(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=14 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-12) &
pids+=" $!"
(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=15 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-13) &
pids+=" $!"
(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=16 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-14) &
pids+=" $!"
(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=17 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-15) &
pids+=" $!"
(docker run -v ~/CP/code:/Code --user=$user --shm-size=2g --cpuset-cpus=18 --cpuset-mems=0 --entrypoint=/bin/bash cellprofiler:optimized -c "/Code/CellProfiler/cellprofiler/cppOptimizations/tests/startZernike.sh" 2>&1 | tee log-zernike-16) &
pids+=" $!"


for p in $pids; do
	echo "Process $p started"
done


# Wait for all processes to finnish, will take max 14s
for p in $pids; do
	if wait $p; then
		echo "Process $p success"
	else
		echo "Process $p fail"
	fi
done

date
