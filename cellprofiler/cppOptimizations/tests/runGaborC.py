import numpy as np
import sys
import scipy.ndimage as scind


sys.path.insert(0,'../../..')
import cellprofiler.cppOptimizations.cppOptimizations.core as gaborC
from cellprofiler.cpmath.filter import gabor,gaborOptim
from cellprofiler.cpmath.cpmorphology import centers_of_labels
from cellprofiler.cpmath.cpmorphology import fixup_scipy_ndimage_result as fix

import time


def runGaborPython(image, labels, scale, gabor_angles):
  
    object_count = np.max(labels)
    best_score = np.zeros((object_count,))

    for angle in range(gabor_angles):
        theta = np.pi * angle / 4
        g = gabor(image, labels, scale, theta)

        score_r = fix(scind.sum(g.real, labels, np.arange(object_count, dtype=np.int32)+ 1))
        score_i = fix(scind.sum(g.imag, labels, np.arange(object_count, dtype=np.int32)+ 1))
        score = np.sqrt(score_r**2+score_i**2)
        best_score = np.maximum(best_score, score)

    # np.savetxt("/home/nick/referenceGabor.csv", best_score, delimiter=",")
    return best_score


def runGaborCPP(image, labels, scale, gabor_angles):
    object_count = np.max(labels)
    centers = centers_of_labels(labels)
    areas = fix(scind.sum(np.ones(image.shape),labels, np.arange(object_count, dtype=np.int32)+1))

    best_score = np.zeros((object_count,))
    gaborC.getGaborScore(image, labels, object_count, scale, gabor_angles, centers, areas, best_score)
    return best_score
        
    

    
def runGaborPythonOptimized(image, labels, scale, gabor_angles):
    object_count = np.max(labels)
    best_score = np.zeros((object_count,))

    centers = centers_of_labels(labels)
    areas = fix(scind.sum(np.ones(image.shape),labels, np.arange(object_count, dtype=np.int32)+1))
    mask = labels > 0
    
    #print mask.shape
    #print mask.dtype
    #print mask[20:30,50:60]
    i,j = np.mgrid[0:image.shape[0],0:image.shape[1]].astype(float)
    #print i.shape
    
    i = i[mask]
    j = j[mask]
    #print i.shape
    image2 = image[mask]
    lm = labels[mask] - 1
    i -= centers[0,lm]
    j -= centers[1,lm]
    #print i.dtype

    sigma = np.sqrt(areas/np.pi) / 3.0
    #print "areas shape: ", areas.shape
    sigma = sigma[lm]
    #print "areas shape: ", areas.shape
    g_exp = 1000.0/(2.0*np.pi*sigma**2) * np.exp(-(i**2 + j**2)/(2*sigma**2))
    for angle in range(gabor_angles):
        theta = np.pi * angle / 4
        # print theta
        g = gaborOptim(image2, labels, scale, theta, object_count, areas,mask, i, j, lm, sigma, g_exp)
        #g = gaborC.runGabor(image, labels, scale, theta, object_count, areas,mask, i, j, lm, sigma, g_exp)
        #g = gaborC.runGabor(image2, labels, scale, theta, object_count, areas,mask, i, j, lm, sigma, g_exp)
        score_r = fix(scind.sum(g.real, labels, np.arange(object_count, dtype=np.int32)+ 1))
        score_i = fix(scind.sum(g.imag, labels, np.arange(object_count, dtype=np.int32)+ 1))
        score = np.sqrt(score_r**2+score_i**2)
        best_score = np.maximum(best_score, score)
    # np.savetxt("/home/nick/pythonOpttimGabor.csv", best_score, delimiter=",")
    return best_score


def main():
    #labels = np.array(([0,1,1,0],[0,1,0,0],[0,0,0,0],[0,0,2,2],[0,0,2,2]), dtype=np.bool)
    image = np.load("image_theta_0.0_scale_5_.npy")
    labels = np.load("labels_theta_0.0_scale_5_.npy").astype("int16")
    scale = 5.0
    gabor_angles = 4
    numIter = 1

    start = time.time()
    for i in range(0,numIter,1):
        score = runGaborPython(image, labels, scale, gabor_angles)
        # print score
        np.savetxt("referenceGabor.csv", score, delimiter=",")
    end = time.time()
    nSecondsGabor = (end - start) / numIter

    print "Mean Exec Time Reference Gabor Python:",nSecondsGabor,"seconds"



    start = time.time()
    for i in range(0,numIter,1):
        score = runGaborCPP(image, labels, scale, gabor_angles)
        # print score
        np.savetxt("cppGabor.csv", score, delimiter=",")
    end = time.time()
    nSecondsGaborOptimC = (end - start) / numIter
    print "Mean Exec Time Gabor Optimized C++:",nSecondsGaborOptimC,"seconds"
    
    print "Speedup:",nSecondsGabor / nSecondsGaborOptimC ,"seconds"


    # start = time.time()
    # for i in range(0,numIter,1):
    #     score = runGaborPythonOptimized(image, labels, scale, gabor_angles)
    # end = time.time()
    # nSecondsGaborOptimPy = (end - start) / numIter
    # print "Mean Exec Time Gabor Optimized Python:",nSecondsGaborOptimPy,"seconds"
    # print "Speedup:",nSecondsGabor / nSecondsGaborOptimPy ,"seconds"

if __name__ == "__main__": main()