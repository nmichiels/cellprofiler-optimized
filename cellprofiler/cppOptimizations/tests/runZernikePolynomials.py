import numpy as np
import sys
import scipy.ndimage as scind


sys.path.insert(0,'../../..')
import cellprofiler.cppOptimizations.cppOptimizations.zernike as zernikeC
from cellprofiler.cpmath.cpmorphology import minimum_enclosing_circle,fixup_scipy_ndimage_result
import cellprofiler.cpmath.zernike as cpmz
import time

ZERNIKE_N = 9


def zernikePython(zernike_indexes,labels,indexes):
    return cpmz.zernike(zernike_indexes,labels,indexes)
    

def zernikeCpp(zernike_indexes,labels,indexes):
    indexes = np.array(indexes,dtype=np.int32)
    nindexes = len(indexes)
    centers,radii = minimum_enclosing_circle(labels,indexes)

    score = np.zeros((nindexes, len(zernike_indexes)))
    zernikeC.construct_zernike_polynomials(zernike_indexes, labels, nindexes, centers, radii, score)
    return score
    

def main():

    labels = np.load("zernikeData/labels.npy").astype("int16")
    indexes = np.load("zernikeData/indexes.npy")
    indexes = np.array(indexes,dtype=np.int32)
    nindexes = len(indexes)

    zernike_indexes = cpmz.get_zernike_indexes(ZERNIKE_N+1).astype("int16")
    # zernike_indexes = zernike_indexes[0:2]
   
    numIter = 1

    #C APPROACH
    start = time.time()
    scoreC = np.zeros((nindexes, len(zernike_indexes)))
    for i in range(0,numIter,1):
        scoreC = zernikeCpp(zernike_indexes,labels,indexes)
    end = time.time()
    nSecondsCpp = (end - start) / numIter
    np.savetxt("zernike_cpp.csv", scoreC, delimiter=",")
    print "Mean Exec Time C++:",nSecondsCpp,"seconds"



    #PYTHON APPROACH
    start = time.time()
    score = np.zeros((nindexes, len(zernike_indexes)))
    for i in range(0,numIter,1):
        score = zernikePython(zernike_indexes,labels,indexes)
    end = time.time()
    nSecondsPython = (end - start) / numIter
    np.savetxt("zernike_python.csv", score, delimiter=",")
    print "Mean Exec Time Python:",nSecondsPython,"seconds" 

    print "Speedup:",nSecondsPython / nSecondsCpp


    
if __name__ == "__main__": main()