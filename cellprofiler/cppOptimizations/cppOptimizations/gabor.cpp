#include <stdio.h>
#include <iostream>
#include "gabor.h"
#include "sinusLUT.h"
#include <vector>

#include <chrono>
#include <ctime>


// #define TIMINGS
// #include <complex>
// #include <cmath>
// using namespace std::complex_literals;

// static SinusLUT lut;

Matrixd calculateMeanPerLabel(){

}

Eigen::Vector2d unit_vector ( double angle ) {
    Eigen::Vector2d r;
   // #  if defined (__i386__) && !defined (NO_ASM)
    #    if defined __GNUC__
    #      define ASM_SINCOS
            std::cout << "asm sincos" << std::endl;
            asm ("fsincos" : "=t" (r[0]), "=u" (r[1]) : "0" (angle));
    #    elif defined _MSC_VER
            define ASM_SINCOS
        __asm fld angle
        __asm fsincos
        __asm fstp r[0]
            __asm fstp r[1]
    #    endif
    // #  endif
        
    //and the fall-back version in C
        
    #  ifndef ASM_SINCOS
        std::cout << "c++ sincos" << std::endl;
        r[0] = cos(angle);
        r[1] = sin(angle);
    #  endif
        return r;
}


// void printSinus(float x){
//     float testCos;
//     float testSin;
//     lut.sincos(x, &testCos, &testSin);
//     std::cout << "x " << x << "  SIN: " << sin(x) << ",  LUT: " << testSin << "  COSIN: " << cos(x) << ",  LUT: " << testCos << std::endl;
// }


template<typename CONSTMAPMAT, typename CONSTMAPVEC, typename T, typename MAT>
void getGaborScore(CONSTMAPMAT & image, ConstMapMatrixs & labels, int nobjects, float frequency, int nAngles, CONSTMAPMAT & centers, CONSTMAPVEC & areas, MapVecd& best_score){
    // T id = 0.0;
    // std::cout << sizeof(id) << std::endl;
    // std::vector<double> twoSigmaSquared;
    // std::vector<double> sigmaConstant;
    // twoSigmaSquared.reserve(areas.size());
    // sigmaConstant.reserve(areas.size());
    // for (unsigned lm = 0; lm < areas.size(); ++lm){
    //     double sigma = sqrt(areas[lm] / M_PI) / 3.0;
    //     twoSigmaSquared.emplace_back(2.0*sigma*sigma);
    //     sigmaConstant.emplace_back(1000.0/(2.0*M_PI*sigma*sigma));
    // }
       
    // printSinus(5.0);
    // printSinus(0.13);
    // printSinus(70.1);
    // printSinus(-1.45225);
    // printSinus(M_PI);
    // printSinus(2.0*M_PI);
    // printSinus(2.0*M_PI-0.0001);

    // std::cout << "asm cos= " << unit_vector(5.0)[0] << ", c++ cos " << cos(5.0) << std::endl;
    // std::cout << "taylor sin= " << sine(5.0) << ", c++ sin " << sin(5.0) << std::endl;
    // std::cout << "taylor cos= " << sine(5.0 + M_PI / 2.0) << ", c++ cos " << cos(5.0) << std::endl;

#ifdef TIMINGS
    auto start = std::chrono::steady_clock::now();
#endif



    std::vector<T> meanImagePerLabel;
    meanImagePerLabel.resize(nobjects, 0.0);


    MAT meanCosPerLabel = MAT::Zero(nobjects, nAngles);
    MAT meanSinPerLabel  = MAT::Zero(nobjects, nAngles);
    std::vector<unsigned> meanCount = std::vector<unsigned>(nobjects, 0);

    MAT gabor_cos = MAT(image.rows()* image.cols(), nAngles);
    MAT gabor_sin = MAT(image.rows()* image.cols(), nAngles);

    // std::cout << "areas: " << areas.rows() << ", " << areas.cols() << std::endl;
    // //std::cout << centers << std::endl;
    // for (unsigned i = 0; i < areas.size(); ++i){
    //     std::cout << " " << areas[i];
    // }
    // meanImagePerLabel = std::vector<double>(nobjects, 0.0);
    // meanCosPerLabel = std::vector<double>(nobjects, 0.0);
    // meanSinPerLabel = std::vector<double>(nobjects, 0.0);
    // meanCount = std::vector<unsigned>(nobjects, 0);
    

    // speedup of 2
    std::vector<T> theta_cos, theta_sin;
    theta_cos.reserve(nAngles);
    theta_sin.reserve(nAngles);
    for (unsigned angle = 0; angle < nAngles; ++angle){
            float theta = M_PI * T(angle) / T(nAngles);
            float angle_sin, angle_cos;
            //sincos(theta, &angle_sin, &angle_cos);
            std::tie(angle_cos, angle_sin) = lut.sincos(theta);
            theta_cos.emplace_back(angle_cos);
            theta_sin.emplace_back(angle_sin);
    }
    
#ifdef TIMINGS
    auto end = std::chrono::steady_clock::now();
    std::cout  << "elapsed time preprocess: " << std::chrono::duration<double>(end-start).count() << "s\n";
    start = std::chrono::steady_clock::now();
#endif
    unsigned idx = 0;
    for (unsigned r = 0; r < image.rows(); ++r){
        for (unsigned c = 0; c < image.cols(); ++c, ++idx){
        
            short label = labels(r,c);
            // apply mask
            if (label < 1)
                continue;
                
            const short lm = label - 1;
            meanCount[lm] += 1;
            meanImagePerLabel[lm] += image(r,c);
            T i = T(r) - centers(0,lm);
            T j = T(c) - centers(1,lm);

            T sigma = sqrt(areas[lm] / M_PI) / 3.0;
            
            T twoSimgaSquared = 2.0*sigma*sigma;
            T g_exp = 1000.0/(twoSimgaSquared*M_PI) * exp(-(i*i + j*j)/(twoSimgaSquared));
            
            // #pragma GCC ivdep
            for (unsigned angle = 0; angle < nAngles; ++angle){
                //double theta = M_PI * double(angle) / double(nAngles);
                T g_angle = 2.0f*M_PI/frequency*(i*theta_cos[angle]+j*theta_sin[angle]);
                // std::cout << g_angle << std::endl;
                float g_cos;
                float g_sin;
                //double g_sin = g_exp * sin(g_angle);

                //asm ("fsincos" : "=t" (g_cos), "=u" (g_sin) : "0" (g_angle));
                // sincos(g_angle, &g_sin, &g_cos);
                // g_sin = std::sin(g_angle);
                // g_cos = std::cos(g_angle);
                //g_sin = lut.sine(g_angle);
                std::tie(g_cos, g_sin) = lut.sincos(g_angle);
                // std::cout << "sincos: " << g_cos << std::endl;
                // g_cos = lut.cosine(g_angle);
                // std::cout << "cos: " << g_cos << std::endl << std::endl;
                
                //g_sin = sine(g_angle);
                //g_cos = sine(g_angle + M_PI / 2.0);

                g_cos*= g_exp;
                g_sin*= g_exp;
                meanCosPerLabel(lm,angle) += g_cos;
                meanSinPerLabel(lm,angle) += g_sin;
                

                gabor_cos(idx, angle) = g_cos;
                gabor_sin(idx, angle) = g_sin;

                
            }
        }
    }

#ifdef TIMINGS
    end = std::chrono::steady_clock::now();
    std::cout  << "elapsed time first loop: " << std::chrono::duration<double>(end-start).count() << "s\n";
    start = std::chrono::steady_clock::now();
#endif

    MAT score_r = MAT::Zero(nobjects, nAngles);
    MAT score_i = MAT::Zero(nobjects, nAngles);
    // std::vector<double> score_r;
    // std::vector<double> score_i;
    // score_r.resize(nobjects, 0.0);
    // score_i.resize(nobjects, 0.0);

    // normalize data based on mean
    idx = 0;
    for (unsigned r = 0; r < image.rows(); ++r){
        for (unsigned c = 0; c < image.cols(); ++c, ++idx){
            short lm = labels(r,c)-1;

            if (lm < 0)
                continue;

            unsigned count = meanCount[lm];

            T i_norm = 0.0;
            if (count > 0){
                i_norm = image(r,c) - meanImagePerLabel[lm] / count;

                for (unsigned angle = 0; angle < nAngles; ++angle){
                    //std::cout << "lm: " << lm << std::endl;
                    
                    T g_cos = 0.0;
                    T g_sin = 0.0;
                    
                    
                    g_cos = gabor_cos(idx, angle) - meanCosPerLabel(lm,angle) / count;
                    g_sin = gabor_sin(idx, angle) - meanSinPerLabel(lm,angle) / count;
                    
                    // std::cout << "before imag" << i_norm  <<std::endl;


                    // std::complex<double> gi;
                    // gi  = i_norm * g_cos + i_norm * g_sin * 1i;
                    score_r(lm, angle) += i_norm * g_cos;
                    score_i(lm,angle) += i_norm * g_sin;
                    //std::cout << "before imag" << i_norm * g_sin <<std::endl;
                    
                    //g.real(i_norm *g_cos);
                    //g.imag(i_norm * g_sin);
                    // std::cout << "aftter imag" << g.imag() <<std::endl;
                
                }
            }

        }
    }
    
#ifdef TIMINGS
    end = std::chrono::steady_clock::now();
    std::cout  << "elapsed time second loop: " << std::chrono::duration<double>(end-start).count() << "s\n";
    start = std::chrono::steady_clock::now();
#endif
    // Matrixd score = Matrixd(1, nobjects);
    unsigned i;
    const unsigned nO = nobjects;
    const unsigned nA = nAngles;
    #pragma GCC ivdep
    for (i = 0; i < nO; ++i){
        T score = 0.0;
        for (unsigned angle = 0; angle < nA; ++angle){
            T newScore = (T) sqrt(score_r(i,angle)*score_r(i,angle) + score_i(i,angle)*score_i(i,angle));
            score = score > newScore ? score : newScore;
        }
        best_score[i] = score;
    }    

#ifdef TIMINGS
    end = std::chrono::steady_clock::now();
    // std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout  << "elapsed time score calculation: " << std::chrono::duration<double>(end-start).count() << "s\n";
#endif

    // return score;
 }


// template void getGaborScore<ConstMapMatrixf, float>(ConstMapMatrixf &, ConstMapMatrixs &, int, float, int, ConstMapMatrixd &, ConstMapVecd &, MapVecd&); // instantiates f<double>(double)


 void getGaborScoreSinglePrec(ConstMapMatrixf & image, ConstMapMatrixs & labels, int nobjects, float frequency, int nAngles, ConstMapMatrixf & centers, ConstMapVecf & areas, MapVecd& best_score){
    getGaborScore<ConstMapMatrixf, ConstMapVecf, float, Matrixf>(image, labels, nobjects, frequency, nAngles, centers, areas, best_score);
}

 void getGaborScoreDoublePrec(ConstMapMatrixd & image, ConstMapMatrixs & labels, int nobjects, float frequency, int nAngles, ConstMapMatrixd & centers, ConstMapVecd & areas, MapVecd& best_score){
    getGaborScore<ConstMapMatrixd, ConstMapVecd, double, Matrixd>(image, labels, nobjects, frequency, nAngles, centers, areas, best_score);
}



//  g_angle = 2*np.pi/frequency*(i*np.cos(theta)+j*np.sin(theta))
//     g_cos = g_exp * np.cos(g_angle)
    
//     g_sin = g_exp * np.sin(g_angle)
//     #
//     # Normalize so that the sum of the filter over each object is zero
//     # and so that there is no bias-value within each object.
//     #   
//     g_cos_mean = fix(scind.mean(g_cos,lm, np.arange(nobjects)))
//     print lm.ndim
//     i_mean = fix(scind.mean(image, lm, np.arange(nobjects)))
//     i_norm = image - i_mean[lm]
//     g_sin_mean = fix(scind.mean(g_sin,lm, np.arange(nobjects)))
//     g_cos -= g_cos_mean[lm] 
//     g_sin -= g_sin_mean[lm]
//     g = np.zeros(mask.shape,dtype=np.complex)
//     g[mask] = i_norm *g_cos+i_norm * g_sin*1j

//     score_r = fix(scind.sum(g.real, labels, np.arange(object_count, dtype=np.int32)+ 1))
//         score_i = fix(scind.sum(g.imag, labels, np.arange(object_count, dtype=np.int32)+ 1))
//         score = np.sqrt(score_r**2+score_i**2)

