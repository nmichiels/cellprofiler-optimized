from cppOptimizations.eigen cimport *

import numpy as np
cimport numpy as np

np.import_array()

cdef extern from "gabor.h":
  cdef void c_getGaborScoreSinglePrec "getGaborScoreSinglePrec" (Map[ConstMatrixf]&, Map[ConstMatrixs]&, int, float, int, Map[ConstMatrixf]&, Map[ConstVecf]&, Map[Vecd]&)
  cdef void c_getGaborScoreDoublePrec "getGaborScoreDoublePrec" (Map[ConstMatrixd]&, Map[ConstMatrixs]&, int, float, int, Map[ConstMatrixd]&, Map[ConstVecd]&, Map[Vecd]&)

def getGaborScore(np.ndarray image, np.ndarray labels, int nobjects, float frequency, int nAngles, np.ndarray centers, np.ndarray areas, np.ndarray best_score):
  if (np.isfortran(image)):
    print("WARNING getGaborScore(): Order of image is Fortran .. expecting C")
  if (np.isfortran(labels)):
    print("WARNING getGaborScore(): Order of labels is Fortran .. expecting C")
    
  if (np.isfortran(areas)):
    print("WARNING getGaborScore(): Order of areas is Fortran .. expecting C")

  if (image.dtype != np.float64):
    print("WARNING getGaborScore(): Type of image is ", image.dtype, "... expecting float64.")
  if (labels.dtype != np.int16):
    print("WARNING getGaborScore(): Type of labels is ", labels.dtype, "... expecting int16.")
  if (centers.dtype != np.float64):
    print("WARNING getGaborScore(): Type of centers is ", centers.dtype, "... expecting float64.")
  if (areas.dtype != np.float64):
    print("WARNING getGaborScore(): Type of areas is ", areas.dtype, "... expecting float64.")
  if (best_score.dtype != np.float64):
    print("WARNING getGaborScore(): Type of best_score is ", best_score.dtype, "... expecting float64.")

  
  labels = labels.astype(dtype=np.int16, order='C', copy=False)
  

  # #double precision
  #image = image.astype(dtype=np.float64, order='C', copy=False)
  #centers = centers.astype(dtype=np.float64, order='C', copy=False)
  #areas = areas.astype(dtype=np.float64, order='C', copy=False)
  #c_getGaborScoreDoublePrec(Map[ConstMatrixd](image), Map[ConstMatrixs](labels), nobjects, frequency, nAngles, Map[ConstMatrixd](centers), Map[ConstVecd](areas), Map[Vecd](best_score))

  # float precision
  image = image.astype(dtype=np.float32, order='C', copy=False)
  centers = centers.astype(dtype=np.float32, order='C', copy=False)
  areas = areas.astype(dtype=np.float32, order='C', copy=False)
  c_getGaborScoreSinglePrec(Map[ConstMatrixf](image), Map[ConstMatrixs](labels), nobjects, frequency, nAngles, Map[ConstMatrixf](centers), Map[ConstVecf](areas), Map[Vecd](best_score))


