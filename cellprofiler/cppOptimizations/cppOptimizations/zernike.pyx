from cppOptimizations.eigen cimport *

import numpy as np
cimport numpy as np

np.import_array()


cdef extern from "zernikePolynomial.h":
  cdef void c_construct_zernike_polynomials "construct_zernike_polynomials" (Map[ConstMatrixs]&, Map[ConstMatrixs]&, int,  Map[ConstMatrixd]&, Map[ConstVecd]&, Map[Matrixd]&)


def construct_zernike_polynomials(np.ndarray zernike_indexes, np.ndarray labels, int numLabels, np.ndarray centers, np.ndarray radii, np.ndarray score):

  if (np.isfortran(zernike_indexes)):
    print("WARNING construct_zernike_polynomials(): Order of zernike_indexes is Fortran .. expecting C")
  if (np.isfortran(labels)):
    print("WARNING construct_zernike_polynomials(): Order of labels is Fortran .. expecting C")
  if (np.isfortran(centers)):
    print("WARNING construct_zernike_polynomials(): Order of centers is Fortran .. expecting C")
  if (np.isfortran(score)):
    print("WARNING construct_zernike_polynomials(): Order of score is Fortran .. expecting C")

  if (zernike_indexes.dtype != np.int16):
    print("WARNING construct_zernike_polynomials(): Type of zernike_indexes is ", zernike_indexes.dtype, "... expecting int16.")
  if (labels.dtype != np.int16):
    print("WARNING construct_zernike_polynomials(): Type of labels is ", labels.dtype, "... expecting int16.")
  if (centers.dtype != np.float64):
    print("WARNING construct_zernike_polynomials(): Type of centers is ", score.dtype, "... expecting float64.")
  if (radii.dtype != np.float64):
    print("WARNING construct_zernike_polynomials(): Type of radii is ", score.dtype, "... expecting float64.")
  if (score.dtype != np.float64):
    print("WARNING construct_zernike_polynomials(): Type of score is ", score.dtype, "... expecting float64.")


  zernike_indexes = zernike_indexes.astype(dtype=np.int16, order='C', copy=False)
  labels = labels.astype(dtype=np.int16, order='C', copy=False)
  centers = centers.astype(dtype=np.float64, order='C', copy=False)
  radii = radii.astype(dtype=np.float64, order='C', copy=False)

  c_construct_zernike_polynomials(Map[ConstMatrixs](zernike_indexes), Map[ConstMatrixs](labels), numLabels, Map[ConstMatrixd](centers), Map[ConstVecd](radii), Map[Matrixd](score))

