#ifndef SINUS_LUT
#define SINUS_LUT

#include <math.h>
#include <vector>

#define LUTSIZE 65536

#define _2_PI 6.28318530717958623200

template<class T>
class SinusLUT {
public:
    SinusLUT(){build();}

    // inline void sincos(T x, T* cos, T* sin) const;
    inline std::pair<T,T> sincos(T x) const;
    inline T sine(T x) const;
    inline T cosine(T x) const;
private:
    void build();

    std::vector<T> lut;
};

// template<class T> 
//  void SinusLUT<T>::sincos(T x, T* c, T* s) const {
//     int idxSine = x / _2_PI * LUTSIZE;
//     int idxCosine = idxSine + LUTSIZE / 4; //  x + pi/2 = cosine 
//     idxSine = idxSine > 0 ? idxSine : -idxSine + LUTSIZE/2;
//     idxCosine =idxCosine > 0 ? idxCosine : -idxCosine + LUTSIZE/2;
//     idxSine %= LUTSIZE;
    
//     idxCosine %= LUTSIZE;

//     *s = lut[idxSine];
//     *c = lut[idxCosine];
// }

template<class T> 
 std::pair<T,T> SinusLUT<T>::sincos(T x) const {
    int idxSine = x / _2_PI * LUTSIZE;
    int idxCosine = idxSine + LUTSIZE / 4; //  x + pi/2 = cosine 
    idxSine = idxSine > 0 ? idxSine : -idxSine + LUTSIZE/2;
    idxCosine =idxCosine > 0 ? idxCosine : -idxCosine + LUTSIZE/2;
    idxSine %= LUTSIZE;
    
    idxCosine %= LUTSIZE;

    return {lut[idxCosine], lut[idxSine]};
    // *s = lut[idxSine];
    // *c = lut[idxCosine];
}


template<class T> 
T SinusLUT<T>::sine(T x) const {
    
    // if (x < 0)
    //     x = -x + M_PI;

    // while (x >= _2_PI)
    //     x -= _2_PI;
    // //x = fmod(x,_2_PI);
    
    // double idx = x / _2_PI * lut.size();
    // return (lut[floor(idx)] +lut[ceil(idx)]) / 2.0;

    // if (x < 0)
    //     x = -x + M_PI;

    // while (x >= _2_PI)
    //     x -= _2_PI;
    //x = fmod(x,_2_PI);
    
    int idx = x / _2_PI * LUTSIZE;

    idx = idx > 0 ? idx : -idx + LUTSIZE/2;
    idx %= LUTSIZE;
    return (lut[idx]);// + lut[idx + 1]);
}

template<class T> 
T SinusLUT<T>::cosine(T x) const {
    return sine(x + M_PI_2);
}

template<class T> 
void SinusLUT<T>::build(){
    lut = std::vector<T>(LUTSIZE);

    for (unsigned i = 0; i < lut.size(); ++i)
        lut[i] = sin(_2_PI * (i / T(lut.size())));
}




#endif
