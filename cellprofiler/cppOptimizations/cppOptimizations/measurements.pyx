from cppOptimizations.eigen cimport *

import numpy as np
cimport numpy as np

np.import_array()


cdef extern from "extrema.h":
  cdef void c_normalizedPerObjectSinglePrec "normalizedPerObjectSinglePrec" (Map[ConstMatrixf]&, Map[ConstMatrixs]&, int, Map[Matrixf]&)
  cdef void c_normalizedPerObjectDoublePrec "normalizedPerObjectDoublePrec" (Map[ConstMatrixd]&, Map[ConstMatrixs]&, int, Map[Matrixd]&)


def normalized_per_object(np.ndarray image, np.ndarray labels, int nobjects, np.ndarray normalized):
  if (np.isfortran(image)):
    print("WARNING extrema(): Order of image is Fortran .. expecting C")
  if (np.isfortran(labels)):
    print("WARNING extrema(): Order of labels is Fortran .. expecting C")
  if (np.isfortran(normalized)):
    print("WARNING extrema(): Order of normalized is Fortran .. expecting C")
    

  if (image.dtype != np.float32):
    print("WARNING extrema(): Type of image is ", image.dtype, "... expecting float32.")
  if (labels.dtype != np.int16):
    print("WARNING extrema(): Type of labels is ", labels.dtype, "... expecting int16.")
  if (normalized.dtype != np.float32):
    print("WARNING extrema(): Type of normalized is ", normalized.dtype, "... expecting float32.")
 
  
  labels = labels.astype(dtype=np.int16, order='C', copy=False)

  # #double precision
  #image = image.astype(dtype=np.float64, order='C', copy=False)
  #normalized = normalized.astype(dtype=np.float64, order='C', copy=False)
  #c_normalizedPerObjectDoublePrec(Map[ConstMatrixd](image), Map[ConstMatrixs](labels), nobjects, Map[Matrixd](normalized))

  # float precision
  image = image.astype(dtype=np.float32, order='C', copy=False)
  normalized = normalized.astype(dtype=np.float32, order='C', copy=False)
  c_normalizedPerObjectSinglePrec(Map[ConstMatrixf](image), Map[ConstMatrixs](labels), nobjects, Map[Matrixf](normalized))
