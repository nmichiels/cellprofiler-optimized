#ifndef GABOR_H
#define GABOR_H

#include "matrixVector.h"

#include <stdio.h>
#include <iostream>
#include "gabor.h"
#include "sinusLUT.h"
#include <vector>

// #include <complex>
// #include <cmath>
// using namespace std::complex_literals;

static SinusLUT<float> lut;



template<typename CONSTMAPMAT, typename CONSTMAPVEC, typename T, typename MAT>
void getGaborScore(CONSTMAPMAT & image, ConstMapMatrixs & labels, int nobjects, float frequency, int nAngles, CONSTMAPMAT & centers, CONSTMAPVEC & areas, MapVecd& best_score);



 void getGaborScoreSinglePrec(ConstMapMatrixf & image, ConstMapMatrixs & labels, int nobjects, float frequency, int nAngles, ConstMapMatrixf & centers, ConstMapVecf & areas, MapVecd& best_score);
 void getGaborScoreDoublePrec(ConstMapMatrixd & image, ConstMapMatrixs & labels, int nobjects, float frequency, int nAngles, ConstMapMatrixd & centers, ConstMapVecd & areas, MapVecd& best_score);

#endif
