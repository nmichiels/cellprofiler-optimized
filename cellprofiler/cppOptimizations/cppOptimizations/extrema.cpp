#include <stdio.h>
#include <iostream>
#include "extrema.h"
#include <limits>
#include <vector>
#include <iomanip>

template<typename CONSTMAPMAT, typename CONSTMAPVEC, typename T, typename MAT, typename MAPMAT>
void normalizedPerObject(CONSTMAPMAT & image, ConstMapMatrixs & labels, int numObjects, MAPMAT& normalized){
	
    
    

    std::vector<T> minImg(numObjects+1, std::numeric_limits<T>::max());
    std::vector<T> maxImg(numObjects+1, std::numeric_limits<T>::min());

    for (unsigned row = 0; row < image.rows(); ++row){
        for (unsigned col = 0; col < image.cols(); ++col){
            
            const short label = labels(row, col);

            T imageVal = image(row,col);
            if (imageVal < minImg[label])
                minImg[label] = imageVal;
            if (imageVal > maxImg[label])
                maxImg[label] = imageVal;

        }
    }

    // for (unsigned label = 0; label < minImg.size(); ++label){
    //     std::cout << std::setprecision(10) << minImg[label] << " , " << maxImg[label] << std::endl;
    // }

    for (unsigned row = 0; row < image.rows(); ++row){
        for (unsigned col = 0; col < image.cols(); ++col){
            const short label = labels(row, col);

            T maxVal = maxImg[label];
            T minVal = minImg[label];

            T division = maxVal > minVal ? maxVal - minVal : 1.0f;
            normalized(row,col) = (image(row,col) - minVal) / division;
        }
    }
}


void normalizedPerObjectSinglePrec(ConstMapMatrixf & image, ConstMapMatrixs & labels, int nobjects, MapMatrixf & normalized){
    normalizedPerObject<ConstMapMatrixf, ConstMapVecf, float, Matrixf, MapMatrixf>(image, labels, nobjects, normalized);
}

void normalizedPerObjectDoublePrec(ConstMapMatrixd & image, ConstMapMatrixs & labels, int nobjects, MapMatrixd & normalized){
    normalizedPerObject<ConstMapMatrixd, ConstMapVecd, double, Matrixd, MapMatrixd>(image, labels, nobjects, normalized);
}


