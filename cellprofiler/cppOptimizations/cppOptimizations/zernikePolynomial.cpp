#include <stdio.h>
#include <iostream>
#include "zernikePolynomial.h"
#include <vector>
#include <map>
#include <complex>
#include <cmath>

Matrixd construct_zernike_lookuptable(ConstMapMatrixs& zernike_indexes){

    std::vector<double> factorial;
    factorial.reserve(100);
    factorial.emplace_back(1);
    double fact = 1.0f;
    for (unsigned i = 1; i < 100; ++i){
        fact *= i;
        factorial.emplace_back(fact);
    }

    unsigned width = zernike_indexes(zernike_indexes.rows()-1, 0) / 2 + 1; // BEWARE: this should be max of first row. Normally they are arranged from small to large, thus we can take the last one

    Matrixd lut = Matrixd::Zero(zernike_indexes.rows(), width);
    for (unsigned idx = 0; idx < zernike_indexes.rows(); ++idx){
        const short n = zernike_indexes(idx,0);
        const short m = zernike_indexes(idx,1);
        const short r = (n-m) / 2 + 1;

        int sign = 1;
        for (unsigned k = 0; k < r; ++k){
            lut(idx, k) = sign * factorial[n-k] / (factorial[k]*factorial[(n+m)/2-k]*factorial[(n-m)/2-k]);
            sign = -sign;
        }
    }

    return lut;

}

typedef Eigen::Matrix<std::complex<double>, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> MatrixComplexd;


void construct_zernike_polynomials_old(ConstMapMatrixs& zernike_indexes, ConstMapMatrixs & labels, int numLabels, ConstMapMatrixd & centers, ConstMapVecd & radii, MapMatrixd & score){
    std::cout << "Constructing Zernike Polynomials\n" << std::flush;
    // std::cout << "zernike_indexes  " << zernike_indexes << std::endl;

    // Matrixd xIn(labels.rows() *  labels.cols(), 2);
    // Matrixd yIn(labels.rows() * labels.cols());
    
    Vecd areas = radii.cwiseProduct(radii) * M_PI;

    const Matrixd lut = construct_zernike_lookuptable(zernike_indexes);

    // std::cout << "Lookuptable" << std::endl;
    // std::cout << lut << std::endl;

    unsigned nzernikes = zernike_indexes.rows();
    
    const unsigned maxM = zernike_indexes(zernike_indexes.rows()-1,0) + 1; // last element should be largest m of whole row
    unsigned c,m;

    std::vector<MatrixComplexd> exp_terms(maxM, MatrixComplexd::Zero(labels.rows(), labels.cols()));

    //debug
    // Matrixd phiDebug = Matrixd::Zero(xIn.rows(), xIn.cols());
    Matrixd radial = Matrixd(labels.rows(), labels.cols());
    unsigned i;
    
    #pragma GCC ivdep
    for (unsigned r = 0; r < labels.rows(); ++r){
        for (unsigned c = 0; c <labels.cols(); ++c){
            
            const short lm = labels(r,c) - 1;

            if (lm < 0)
                continue;

            double rad = radii[lm];
            double y = (r - centers(lm,0)) / rad;
            double x = (c - centers(lm,1)) / rad;

            radial(r,c) = std::sqrt(x*x+y*y);
           
            double phi = atan2(x,y);
            // phiDebug(r,c) = phi;
            for (m = 0; m < maxM; ++m){
                std::complex<double> expterm;
                expterm.real(0);
                expterm.imag(m*phi);
                exp_terms[m](r,c) = exp(expterm);

            }
        }
    }

    
  

    
    

    
    #pragma GCC ivdep
    for (unsigned idx = 0; idx < zernike_indexes.rows(); ++idx){
       
        const short n = zernike_indexes(idx,0);
        const short m = zernike_indexes(idx,1);
        const short range = (n-m) / 2 + 1;
        
        const MatrixComplexd& exp_term = exp_terms[m];

        

        
        

        

        // const MatrixComplexd zf = s*exp_terms[m];//(xIn.rows(), xIn.cols());

        
       
        // std::cout << "zf size: " << zf.size() << std::endl;
        // std::cout << "exp_terms size: " << exp_terms.size() << std::endl;
        // std::cout << "exp_term size: " << exp_terms[m].size() << std::endl;

        Vecd real_score = Vecd::Zero(numLabels);
        Vecd imag_score = Vecd::Zero(numLabels);
        // MatrixComplexd zf = MatrixComplexd::Zero(xIn.rows(), xIn.cols());
        #pragma GCC ivdep
        for (unsigned row = 0; row < labels.rows(); ++row){
            for (unsigned col = 0; col <labels.cols(); ++col){
                    const short lm = labels(row, col) - 1;

                    if (lm < 0)
                        continue;
                    
                    // double x = xIn(i);
                    // double y = yIn(i);
                    const double r = radial(row, col);//std::sqrt(x*x+y*y);
                    
                    std::complex<double> s(0.0, 0.0);
                    for (unsigned k = 0; k < range; ++k)
                        s += lut(idx,k) * std::pow(r, (n-2*k));
                    s = r > 1 ? 0 : s;

                    std::complex<double> zf = s*exp_terms[m](row, col);//(xIn.rows(), xIn.cols());

                    

                    real_score[lm] += zf.real();
                    imag_score[lm] += zf.imag();
            }
        }
        
        // std::cout << "zf" << std::endl;
        // for (unsigned r = 500; r < 505; ++r){
        //     for (unsigned c = 500; c < 507; ++c){
        //         std::cout << "  " <<  zf(r,c).real() << "+" << zf(r,c).imag() << "j" ;
        //         // std::cout << "  " <<  phiDebug(r,c);// << "+" << mask(r,c) << "j" ;
        //     }
        //     std::cout << std::endl;
        // }
        // std::cout << std::endl;
        //  for (unsigned r = 0; r < 5; ++r){
        //      std::cout << "  " << real_score(r);            
        //     }
        //             std::cout << std::endl;

       

        score.col(idx) = (real_score.cwiseProduct(real_score)+ imag_score.cwiseProduct(imag_score)).cwiseSqrt().cwiseQuotient(areas);
        // std::cout << "result: " << result << std::endl;

        // real_score = scipy.ndimage.sum(zfk.real,labels,indexes)
        // real_score = fixup_scipy_ndimage_result(real_score)
            
        // imag_score = scipy.ndimage.sum(zfk.imag,labels,indexes)
        // imag_score = fixup_scipy_ndimage_result(imag_score)
        // one_score = np.sqrt(real_score**2+imag_score**2) / areas
        // score[:,ki] = one_score
        
    }
    std::cout << "Done Constructing Zernike Polynomials\n" << std::flush;
    // r = np.sqrt(x**2+y**2)
    // phi = np.arctan2(x,y).astype(np.complex)
    // zf = np.zeros((x.shape[0], nzernikes), np.complex)
    // s = np.zeros(x.shape,np.complex)
    // exp_terms = {}
    // for idx,(n,m) in zip(range(nzernikes), zernike_indexes):
    //     s[:]=0
    //     if not exp_terms.has_key(m):
    //         exp_terms[m] = np.exp(1j*m*phi)
    //     exp_term = exp_terms[m]
    //     for k in range((n-m)/2+1):
    //         s += lut[idx,k] * r**(n-2*k)
    //     s[r>1]=0
    //     zf[:,idx] = s*exp_term 
    
    // result = np.zeros((mask.shape[0],mask.shape[1],nzernikes),np.complex)
    // result[mask] = zf



    // radii = np.array(radii)
    // k = zf.shape[2]
    // n = np.product(radii.shape)
    // score = np.zeros((n,k))
    // if n == 0:
    //     return score
    // areas = radii**2 * np.pi
    // for ki in range(k):
    //     zfk=zf[:,:,ki]
    //     real_score = scipy.ndimage.sum(zfk.real,labels,indexes)
    //     real_score = fixup_scipy_ndimage_result(real_score)
            
    //     imag_score = scipy.ndimage.sum(zfk.imag,labels,indexes)
    //     imag_score = fixup_scipy_ndimage_result(imag_score)
    //     one_score = np.sqrt(real_score**2+imag_score**2) / areas
    //     score[:,ki] = one_score
    // return score
    
}


void construct_zernike_polynomials(ConstMapMatrixs& zernike_indexes, ConstMapMatrixs & labels, int numLabels, ConstMapMatrixd & centers, ConstMapVecd & radii, MapMatrixd & score){
    // std::cout << "Constructing Zernike Polynomials\n" << std::flush;
    // std::cout << "zernike_indexes  " << zernike_indexes << std::endl;

    // Matrixd xIn(labels.rows() *  labels.cols(), 2);
    // Matrixd yIn(labels.rows() * labels.cols());
    
    Vecd areas = radii.cwiseProduct(radii) * M_PI;

    const Matrixd lut = construct_zernike_lookuptable(zernike_indexes);

    // std::cout << "Lookuptable" << std::endl;
    // std::cout << lut << std::endl;

    unsigned nzernikes = zernike_indexes.rows();
    
    // const unsigned maxM = zernike_indexes(zernike_indexes.rows()-1,0) + 1; // last element should be largest m of whole row
    // unsigned c,m;

    // std::vector<MatrixComplexd> exp_terms(maxM, MatrixComplexd::Zero(labels.rows(), labels.cols()));

    //debug
    // Matrixd phiDebug = Matrixd::Zero(xIn.rows(), xIn.cols());
    // Matrixd radial = Matrixd(labels.rows(), labels.cols());
    // unsigned i;
    
    // #pragma GCC ivdep
    // for (unsigned r = 0; r < labels.rows(); ++r){
    //     for (unsigned c = 0; c <labels.cols(); ++c){
            
    //         const short lm = labels(r,c) - 1;

    //         if (lm < 0)
    //             continue;

    //         double rad = radii[lm];
    //         double y = (r - centers(lm,0)) / rad;
    //         double x = (c - centers(lm,1)) / rad;

    //         radial(r,c) = std::sqrt(x*x+y*y);
           
    //         double phi = atan2(x,y);
    //         // phiDebug(r,c) = phi;
    //         for (m = 0; m < maxM; ++m){
    //             std::complex<double> expterm;
    //             expterm.real(0);
    //             expterm.imag(m*phi);
    //             exp_terms[m](r,c) = exp(expterm);

    //         }
    //     }
    // }

    
  

    std::map<short, MatrixComplexd> exp_terms;


    
    #pragma GCC ivdep
    for (unsigned idx = 0; idx < zernike_indexes.rows(); ++idx){
       
        const short n = zernike_indexes(idx,0);
        const short m = zernike_indexes(idx,1);
        const short range = (n-m) / 2 + 1;
        
        // const MatrixComplexd& exp_term = exp_terms[m];

        
        bool expTermExists;
        MatrixComplexd* exp_term;
        if ( exp_terms.find(m) == exp_terms.end() ) {
            // std::cout << " not found\n";
            expTermExists = false;// found
            exp_terms[m] = MatrixComplexd(labels.rows(), labels.cols());
            exp_term = &exp_terms[m];
        } else {
            // std::cout << "found\n";
            expTermExists = true;// not found
            exp_term = &exp_terms[m];
        }
        
        // std::cout << "m: " << m << std::endl;
        // std::cout << exp_term->rows() << ", " << exp_term->cols() << std::endl;

        // const MatrixComplexd zf = s*exp_terms[m];//(xIn.rows(), xIn.cols());

        
       
        // std::cout << "zf size: " << zf.size() << std::endl;
        // std::cout << "exp_terms size: " << exp_terms.size() << std::endl;
        // std::cout << "exp_term size: " << exp_terms[m].size() << std::endl;

        Vecd real_score = Vecd::Zero(numLabels);
        Vecd imag_score = Vecd::Zero(numLabels);
        // MatrixComplexd zf = MatrixComplexd::Zero(xIn.rows(), xIn.cols());
        #pragma GCC ivdep
        for (unsigned row = 0; row < labels.rows(); ++row){
            for (unsigned col = 0; col <labels.cols(); ++col){
                    const short lm = labels(row, col) - 1;

                    if (lm < 0)
                        continue;
                    

                    double rad = radii[lm];
                    double y = (row - centers(lm,0)) / rad;
                    double x = (col - centers(lm,1)) / rad;

                    const double r  = std::sqrt(x*x+y*y);
                
                    
                
                    
                    std::complex<double> s(0.0, 0.0);
                    for (unsigned k = 0; k < range; ++k)
                        s += lut(idx,k) * std::pow(r, (n-2*k));
                    s = r > 1 ? 0 : s;

                    std::complex<double> zf;
                    if (expTermExists){
                        zf = s*(*exp_term)(row, col);//(xIn.rows(), xIn.cols());
                    }
                    else {
                        double phi = atan2(x,y);
                        // std::cout << phi << " ";
                        std::complex<double> ex;
                        ex.real(0);
                        ex.imag(m*phi);
                        ex = exp(ex);
                        (*exp_term)(row, col) = ex;
                        zf = s*ex;//(xIn.rows(), xIn.cols());
                    }

                    

                    real_score[lm] += zf.real();
                    imag_score[lm] += zf.imag();
            }
        }
        
        // std::cout << "zf" << std::endl;
        // for (unsigned r = 500; r < 505; ++r){
        //     for (unsigned c = 500; c < 507; ++c){
        //         std::cout << "  " <<  zf(r,c).real() << "+" << zf(r,c).imag() << "j" ;
        //         // std::cout << "  " <<  phiDebug(r,c);// << "+" << mask(r,c) << "j" ;
        //     }
        //     std::cout << std::endl;
        // }
        // std::cout << std::endl;
        //  for (unsigned r = 0; r < 5; ++r){
        //      std::cout << "  " << real_score(r);            
        //     }
        //             std::cout << std::endl;

       

        score.col(idx) = (real_score.cwiseProduct(real_score)+ imag_score.cwiseProduct(imag_score)).cwiseSqrt().cwiseQuotient(areas);
        // std::cout << "result: " << result << std::endl;

        // real_score = scipy.ndimage.sum(zfk.real,labels,indexes)
        // real_score = fixup_scipy_ndimage_result(real_score)
            
        // imag_score = scipy.ndimage.sum(zfk.imag,labels,indexes)
        // imag_score = fixup_scipy_ndimage_result(imag_score)
        // one_score = np.sqrt(real_score**2+imag_score**2) / areas
        // score[:,ki] = one_score
        
    }
    // std::cout << "Done Constructing Zernike Polynomials\n" << std::flush;
    // r = np.sqrt(x**2+y**2)
    // phi = np.arctan2(x,y).astype(np.complex)
    // zf = np.zeros((x.shape[0], nzernikes), np.complex)
    // s = np.zeros(x.shape,np.complex)
    // exp_terms = {}
    // for idx,(n,m) in zip(range(nzernikes), zernike_indexes):
    //     s[:]=0
    //     if not exp_terms.has_key(m):
    //         exp_terms[m] = np.exp(1j*m*phi)
    //     exp_term = exp_terms[m]
    //     for k in range((n-m)/2+1):
    //         s += lut[idx,k] * r**(n-2*k)
    //     s[r>1]=0
    //     zf[:,idx] = s*exp_term 
    
    // result = np.zeros((mask.shape[0],mask.shape[1],nzernikes),np.complex)
    // result[mask] = zf



    // radii = np.array(radii)
    // k = zf.shape[2]
    // n = np.product(radii.shape)
    // score = np.zeros((n,k))
    // if n == 0:
    //     return score
    // areas = radii**2 * np.pi
    // for ki in range(k):
    //     zfk=zf[:,:,ki]
    //     real_score = scipy.ndimage.sum(zfk.real,labels,indexes)
    //     real_score = fixup_scipy_ndimage_result(real_score)
            
    //     imag_score = scipy.ndimage.sum(zfk.imag,labels,indexes)
    //     imag_score = fixup_scipy_ndimage_result(imag_score)
    //     one_score = np.sqrt(real_score**2+imag_score**2) / areas
    //     score[:,ki] = one_score
    // return score
    
}