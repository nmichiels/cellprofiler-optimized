#ifndef EXTREMA_H
#define EXTREMA_H

#include "matrixVector.h"



template<typename CONSTMAPMAT, typename CONSTMAPVEC, typename T, typename MAT, typename MAPMAT>
void normalizedPerObject(CONSTMAPMAT & image, ConstMapMatrixs & labels, int nobjects, MAPMAT & normalized);



 void normalizedPerObjectSinglePrec(ConstMapMatrixf & image, ConstMapMatrixs & labels, int nobjects, MapMatrixf & normalized);
 void normalizedPerObjectDoublePrec(ConstMapMatrixd & image, ConstMapMatrixs & labels, int nobjects, MapMatrixd & normalized);


#endif
