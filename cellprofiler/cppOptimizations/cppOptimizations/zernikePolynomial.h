#ifndef ZERNIKE_POLYNOMIAL_H
#define ZERNIKE_POLYNOMIAL_H

#include "matrixVector.h"



Matrixd construct_zernike_lookuptable(ConstMapMatrixs& zernike_indexes);

void construct_zernike_polynomials(ConstMapMatrixs& zernike_indexes, ConstMapMatrixs & labels, int numLabels, ConstMapMatrixd & centers, ConstMapVecd & radii, MapMatrixd & zf);



#endif
