from distutils.core import setup
from distutils.extension import Extension
import numpy as np
from Cython.Build import cythonize

__package_name__ = "cppOptimizations"

eigen_include_dir = "/usr/include/eigen3"

extensions = [
    Extension("cppOptimizations.conversion", ["cppOptimizations/conversion.pyx"],
              include_dirs=[np.get_include(), eigen_include_dir],
              language="c++",
              extra_compile_args=["-std=c++11", "-Wno-ignored-attributes", "-Wno-int-in-bool-context"],
              extra_link_args=["-std=c++11"]
              ),
    Extension("cppOptimizations.eigen", ["cppOptimizations/eigen.pyx"],
              include_dirs=[np.get_include(), eigen_include_dir],
              language="c++",
              extra_compile_args=["-std=c++11", "-Wno-ignored-attributes", "-Wno-int-in-bool-context"],
              extra_link_args=["-std=c++11"]
              ),
    Extension("cppOptimizations.core", ["cppOptimizations/core.pyx", "cppOptimizations/gabor.cpp", "cppOptimizations/sinusLUT.cpp"],
              include_dirs=[np.get_include(), eigen_include_dir],
              language="c++",
              extra_compile_args=["-std=c++11", "-march=native", "-mtune=native", "-O3", "-ftree-vectorize", "-fopt-info-all=vect.all"],#, "-fopt-info-vec-missed" ,"-fopt-info" ],
              extra_link_args=["-std=c++11"],
              #libraries=["DiffMEM"],
              #library_dirs=[diffmem_library_path],
                ),
    Extension("cppOptimizations.zernike", ["cppOptimizations/zernike.pyx", "cppOptimizations/zernikePolynomial.cpp"],
              include_dirs=[np.get_include(), eigen_include_dir],
              language="c++",
              extra_compile_args=["-std=c++11", "-march=native", "-mtune=native", "-O3"],
              extra_link_args=["-std=c++11"],
              #libraries=["DiffMEM"],
              #library_dirs=[diffmem_library_path],
                ),
    Extension("cppOptimizations.measurements", ["cppOptimizations/measurements.pyx", "cppOptimizations/extrema.cpp"],
              include_dirs=[np.get_include(), eigen_include_dir],
              language="c++",
              extra_compile_args=["-std=c++11", "-march=native", "-mtune=native", "-O3"],
              extra_link_args=["-std=c++11"],
              #libraries=["DiffMEM"],
              #library_dirs=[diffmem_library_path],
                )
    #           ),
    # Extension("gaborC.zernike", ["gaborC/zernike.pyx", "gaborC/zernike.cpp"],
    #           include_dirs=[np.get_include(), eigen_include_dir],
    #           language="c++",
    #           extra_compile_args=["-std=c++11", "-march=native", "-mtune=native", "-O3"],
    #           extra_link_args=["-std=c++11"],
    #           )
]

dist = setup(
    name=__package_name__,
    version="0.01",
    description="cppOptimizations",
    author="Nick Michiels",
    author_email="nick.michiels@uhasselt.be",
    ext_modules=cythonize(extensions),#, gdb_debug=True),
    packages=[__package_name__], requires=['numpy', 'Cython']
)

